require('./config/config');; 
require('./global_functions'); //instantiate configuration variables

var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
var validator = require('express-validator');
const passport = require('passport');

var seller_product = require('./routes/seller_product');
var admin_manage = require('./routes/admin_manage');


var bodyParser = require('body-parser');
const mongoose = require('mongoose');
var helmet = require('helmet');

var cron = require('cron');

const { sanitizeBody } = require('express-validator/filter');
var app = express();
var DEVICE = 0;

//var debug = require('debug')('express-sq:server');

app.use(helmet());
app.use(helmet.frameguard());
app.use(helmet.noCache());




//app.use(logger('dev'));
app.use(logger(':method :url :status :response-time ms - :res[content-length] :remote-addr'));
/*app.use(express.json());
app.use(express.urlencoded({ extended: false }));*/
app.use(bodyParser.json({ limit: '5mb', defaultCharset: 'utf-8' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));



// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
// mongoose
//   .connect(db)
//   .then(() => console.log('MongoDB Connected'))
//   .catch(err => console.log(err));

// mongoose.set('debug', true);


//Passport
app.use(passport.initialize());

const models = require('./models');
models.sequelize
    .authenticate()
    .then(() => {
        console.log('Connected to SQL database:', CONFIG.db_name);
    })
    .catch(err => {
        console.error(
            'Unable to connect to SQL database:',
            CONFIG.db_name,
            err
        );
    });

if(CONFIG.API_VERSION == CONFIG.API_VERSION2)
    console.log('Current Version: ', CONFIG.API_VERSION);
else
    console.log('Current Version: ', CONFIG.API_VERSION +", "+CONFIG.API_VERSION2);
    
const moment = require('moment'); 

app.use(sanitizeBody(['*']).trim())
app.use(validator());


app.get('/test', function(req, res) {
    return ReS(res, 'done');
});

app.use("/product", seller_product);
app.use("/admin", admin_manage);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500).json({
        message: err.message,
        error: err
    });
});

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

var port = normalizePort(process.env.PORT) || '3000';
app.listen(port, () => console.log(`Server listening on port ${port}`));

module.exports = app;
