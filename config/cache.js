const redis = require('redis');
const util = require('util');
const client = redis.createClient(CONFIG.redisPort, CONFIG.redisUrl);
const shimmer = require('shimmer');

var Sequelize = require('sequelize');

//var queryInterface = Sequelize.getQueryInterface();

//console.log(queryInterface)

client.hget = util.promisify(client.hget);


const WrappingCache = function(model){

   

    model.cache = function ( device_id) {


        this.caching = true;
        this.device_id = device_id;
        return this;

    };

    // model.beforeFind((options) => {

       
    //    // model.caching = false;
    // });

    var rewrite = ['find','findAll'];

    rewrite.forEach( rw => {

        shimmer.wrap(model,rw, function (original){
            

            return async function(options) {
                
                if(!this.caching)
                {
                return original.call(this,options);
                }  

                let hashkey  = {"model":this.name,"options":options};
        
                hashkey = JSON.stringify(hashkey);

                
                let cacheValue = await client.hget(this.device_id,hashkey)

                if(cacheValue)
                {
                    let data = JSON.parse(cacheValue);
        
                    let ret = Array.isArray(data) ? data.map(d=>this.build(d,{isNewRecord: false})): this.build(data,{isNewRecord: false})
            
                    return ret;
                
                }
                
                var result =  await original.call(this,options);

                
            
                client.hset(this.device_id,hashkey,JSON.stringify(result));
                client.expire(this.device_id, CONFIG.redisCacheTime);

                return result;
            }
        
        });



    } );

   
    return model;
}
module.exports.WrappingCache = WrappingCache;


const WrappingSelectCache = function(model){

    

    console.log('hahahahahhw texte');
    console.log(data)
    
    //console.log(arguments);
    //console.log(this);
    
    return select.apply(this,arguments);
     
   


}
module.exports.WrappingSelectCache = WrappingSelectCache;


clearCache = async function(req, res, next) {

    await next();

    client.del(DEVICE);
  
};
module.exports.clearCache = clearCache;

