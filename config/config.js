require('dotenv').config();//instatiate environment variables

CONFIG = {} //Make this global to use all over the application

CONFIG.app          = process.env.APP   || 'dev';
CONFIG.port         = process.env.PORT  || '3000';

CONFIG.db_dialect   = process.env.DB_DIALECT    || 'mysql';
CONFIG.db_host      = process.env.DB_HOST       || 'localhost';
CONFIG.db_port      = process.env.DB_PORT       || '3306';
CONFIG.db_name      = process.env.DB_NAME       || 'name';
CONFIG.db_user      = process.env.DB_USER       || 'root';
CONFIG.db_password  = process.env.DB_PASSWORD   || '';

CONFIG.jwt_encryption_consumer  = process.env.JWT_ENCRYPTION_CONSUMER || 'jwt_please_change';
CONFIG.jwt_encryption_merchant  = process.env.JWT_ENCRYPTION_MERCHANT || 'jwt_please_change';
CONFIG.jwt_encryption_admin  = process.env.JWT_ENCRYPTION_ADMIN || 'jwt_please_change';
CONFIG.jwt_expiration  = process.env.JWT_EXPIRATION || '10000';

CONFIG.redisPort  = process.env.redisPort||6379 ;
CONFIG.redisUrl = process.env.redisUrl||'localhost' ;
CONFIG.redisCacheTime = process.env.redisCacheTime||30 ;

// CONFIG.filePath  = `http://${process.env.HOST}/images/review/` ||'http://192.168.88.158:3100/images/' ;
// CONFIG.storePath  = './public/images/review/';
CONFIG.filePath  = process.env.FILE_PATH || '';
CONFIG.storePath  = process.env.UPLOAD_IMAGE_PATH || 'public/images';

CONFIG.filePathConsumer  = `${process.env.FILE_PATH}/images/` ||'http://192.168.88.163:3007/images/' ;
CONFIG.storePathConsumer  = './public/images/profile/' ;

CONFIG.CLIENT_FCM_SERVER_KEY  = process.env.CLIENT_FCM_SERVER_KEY || '';
CONFIG.MERCHANT_FCM_SERVER_KEY  = process.env.MERCHANT_FCM_SERVER_KEY || '';
CONFIG.RIDER_FCM_SERVER_KEY  = process.env.RIDER_FCM_SERVER_KEY || '';
CONFIG.LOLOL_MERCHANT_FCM_SERVER_KEY  = process.env.LOLOL_MERCHANT_FCM_SERVER_KEY || '';

