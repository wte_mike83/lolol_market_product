'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var Consumer = sequelize.define('Consumer', {
   
    
    consumer_id: {type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    email: DataTypes.STRING,
    ieat_id: {type:DataTypes.STRING,unique:true},
    country_code: DataTypes.STRING,
    phone_no: DataTypes.STRING,
    profile_name: DataTypes.STRING,
    fb_id: DataTypes.STRING,
    gp_id: DataTypes.STRING,
    password: DataTypes.STRING,
    acc_active: DataTypes.STRING,
    email_verification_flag: DataTypes.INTEGER,
    email_verification_code: DataTypes.STRING,
    is_blogger: DataTypes.INTEGER,
    point_collected: DataTypes.INTEGER,
    wallet: DataTypes.DECIMAL,
    wallet_flag: DataTypes.INTEGER,
    wallet_pin: DataTypes.STRING,
    referral_code: DataTypes.STRING,
    refer_from: DataTypes.STRING,
    refer_date: 'TIMESTAMP',
    pre_notification: DataTypes.INTEGER,
    notif_bell: 'TIMESTAMP',
    profile_pic: DataTypes.STRING,
    profile_checksum: DataTypes.STRING,
    food_perf:DataTypes.STRING,
    
  }, {
    freezeTableName: true,
    tableName: 'consumer',
    getterMethods: {
      profile_url:  function() {

        if(this.getDataValue('profile_pic') != null)
          return CONFIG.filePath +'/profile/'+this.getDataValue('profile_pic');
        else
          return CONFIG.filePath +'/profile/default-user.png';

      }
      
    },

  });


  Consumer.associate = function(models) {
   
  };

  Consumer.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    delete json['password'];

    return json;
  };

  Consumer.prototype.getJWT = function (device_id) {
    let expiration_time = parseInt(CONFIG.jwt_expiration);
    return "Bearer "+jwt.sign({consumer_id:this.consumer_id,device_id:device_id}, CONFIG.jwt_encryption, {expiresIn: expiration_time});
  };

  
  
  return Consumer;
};