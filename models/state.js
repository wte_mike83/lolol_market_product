'use strict';
module.exports = (sequelize, DataTypes) => {
  var State = sequelize.define('State', {
    state_id: {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true} ,
    state_name: DataTypes.STRING,
    state_pic_path: DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'state_list',
  });
  
  State.associate = function(models) {
    this.state_id = this.hasMany(models.District, {foreignKey: 'state_id'});
  };
  
  return State;
};