'use strict';
module.exports = (sequelize, DataTypes) => {
  var Category = sequelize.define('Category', {
        category_id: {type:DataTypes.INTEGER.UNSIGNED ,primaryKey: true,autoIncrement: true},
        category_name: DataTypes.STRING,
        category_description: DataTypes.STRING,
        image_path: {type: DataTypes.STRING, allowNull: true,
          get: function () {
              if(this.getDataValue('image_path')){

                if(this.getDataValue('specialPackage_id'))
                {
                  return CONFIG.filePath +'/images/offer/'+ this.getDataValue('specialPackage_id') +'/'+ this.getDataValue('image_path');
                }
                else{
                  return CONFIG.filePath +'/images/category/'+ this.getDataValue('image_path');
                }
                  
              }else if(this.getDataValue('image_path') === undefined)
                  return ;
              else 
                  return CONFIG.filePath + '/images/media/default.png';
              // return null;
          }},
        thumbnail_path: {type: DataTypes.STRING, allowNull: true,
          get: function () {
              if(this.getDataValue('thumbnail_path')){

                if(this.getDataValue('specialPackage_id'))
                {
                  return CONFIG.filePath +'/images/offer/'+ this.getDataValue('specialPackage_id') +'/'+ this.getDataValue('image_path');
                }
                else{
                  return CONFIG.filePath +'/images/category/'+ this.getDataValue('thumbnail_path');
                }
                  
              }else if(this.getDataValue('thumbnail_path') === undefined)
                  return ;
              else 
                  return CONFIG.filePath + '/images/media/default.png';
              // return null;
          }},
        display_index: DataTypes.INTEGER,
        checksum: DataTypes.STRING,
        category_group: DataTypes.STRING,
        categories: DataTypes.STRING,
        publish: DataTypes.INTEGER,
    }, {
        freezeTableName: true,
        tableName: 'categories',
    });

    Category.associate = function(models) {
  //   this.restaurant_id = this.belongsTo(models.restaurant, {foreignKey: 'restaurant_id'});
  //   // this.restaurant_id = this.hasMany(models.RestaurantReservation, {foreignKey: 'restaurant_id'});
    };

    Category.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Category;
};