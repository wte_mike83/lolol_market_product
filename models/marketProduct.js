const moment = require('moment');
'use strict';


module.exports = (sequelize, DataTypes) => {

  var MarketProduct = sequelize.define('MarketProduct', {
   
    
    product_id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    shop_id: DataTypes.INTEGER,
    product_pic: DataTypes.STRING,
    product_name: DataTypes.STRING,
    product_desc: DataTypes.STRING,
    stock: DataTypes.STRING,
    category_id: DataTypes.INTEGER,
    category: DataTypes.STRING,
    weight: DataTypes.DECIMAL,
    shipping_id: DataTypes.INTEGER,
    condition: DataTypes.STRING,
    self_shipping: DataTypes.TINYINT,
    publish: DataTypes.TINYINT,
    sold: DataTypes.INTEGER,
    verify: DataTypes.INTEGER,
    createdAt:'TIMESTAMP',
  
  }, {
    freezeTableName: true,
    tableName: 'market_product',
    timestamps: true, 
    getterMethods: {

      createdAt: function () {
        if(this.getDataValue('createdAt'))
        {
            return moment(this.getDataValue('createdAt'), 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');}
        else if(this.getDataValue('createdAt') === undefined)
            return ;
        else 
          return null;
       }, 
       product_pic:  function() {

        if(this.getDataValue('product_pic') != null)
        {
          
           var pic = JSON.parse(this.getDataValue('product_pic'))

          for ( i = 0; i < pic.length; i++) {
            //console.log("actual"+pic[i].product_img)
            pic[i].product_img = CONFIG.filePath +"product/"+this.getDataValue('shop_id') +"/"+pic[i].product_img;
            pic[i].thumbnail = CONFIG.filePath +"product/"+this.getDataValue('shop_id') +"/"+pic[i].thumbnail;
            pic[i].thumbnail_square = CONFIG.filePath +"product/"+this.getDataValue('shop_id') +"/"+pic[i].thumbnail_square;
            delete pic[i].checkSum; 
          }



          return pic;
        }

       // else
          //return CONFIG.filePath +'profile/default-user.png';

      }
    } 
  });



  return MarketProduct;
};