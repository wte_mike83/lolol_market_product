'use strict';
const jwt    = require('jsonwebtoken');
module.exports = (sequelize, DataTypes) => {

  var Device = sequelize.define('Device', {

    device_id:{type:DataTypes.INTEGER ,primaryKey: true,autoIncrement: true} ,
    app_token:DataTypes.STRING,
    uuid:DataTypes.STRING,
    consumer_id:DataTypes.INTEGER,
    food_perf:DataTypes.STRING,

  }, {
    freezeTableName: true,
    tableName: 'consumer_device',
 

  });

  Device.associate = function(models) {
    
    // associations can be defined here
  };


  Device.prototype.toWeb = function (pw) {
    let json = this.toJSON();
    return json;
  };

  Device.prototype.getJWT = function () {
    let expiration_time = parseInt(28800);
    return "Bearer "+jwt.sign({device_id:this.device_id}, CONFIG.jwt_encryption_consumer, {expiresIn: expiration_time});
  };



  return Device;
};