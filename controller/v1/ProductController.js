const MarketShop   = require('../../models').MarketShop;
const MarketProduct   = require('../../models').MarketProduct;

const State = require('../../models').State;
const District = require('../../models').District;
//const dir = 'C:/project/lolol_seller/controller/v1/public';
const dir = CONFIG.storePath;


const md5File = require('md5-file');
const sharp = require('sharp');


const test = async function (req, res) {
    console.log("hihihihi")

}
module.exports.test = test;




const addProduct = async function (req, res) {

  var checkShopID = Number.isInteger(parseInt(req.params.shop_id))

  if(!checkShopID)
      return ReE(res,'Invalid Shop ID', 400);


    const maxSize = 50 * 1024 * 1024;

    const multer = require('multer');
    const helper = require("./helper");

    const dest = dir + '/product/1';

    var storage = multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, dest)
        },
        filename: (req, file, cb) => {
            cb(null, generateRandomKey(5) + generateRandomKey(5) + generateRandomKey(5) + '.' + file.originalname.split('.').pop())
        }
    });



    var upload = multer({
        storage: storage,
        limits: {
            fileSize: maxSize,
            files: 9
        },

        fileFilter: function (req, file, cb) {
            if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg' && file.mimetype !== 'image/jpg' && file.mimetype !== 'application/octet-stream') {
                return cb(new Error('Only image file are allowed'))
            }
            cb(null, true);
        }
    }).array('image');



    createImageFolder = new Promise((resolve, reject) => {

        helper.checkDirectorySync(dir).then(ret => {

            helper.checkDirectorySync(dest).then(ret => {

                resolve(ret);

            }).catch((err) => {
                reject(err);
            })

        }).catch((err) => {
            reject(err);
        })
    });



    createImageFolder.then((directory) => {

        return upload(req, res, function (err) {
            let files = req.files;

            if (err) {

                return ReE(res, err, 400); // return error

            } else if (err) {

                return ReE(res, err, 400); // return error

            } else {


                if (files.length > 0) {
                    let images = [];
                    var promise = [];

                    convertImage = new Promise((resolve, reject) => {


                        for (i = 0; i < files.length; i++) {


                            function formImage() {

                                return new Promise((resolve, reject) => {

                                    var obj = files[i];
                                    let image = {};

                                    //console.log("loop " + JSON.stringify(obj))

                                    image.product_img = obj.filename;
                                    image.checkSum = md5File.sync(obj.destination + "/" + obj.filename);
                                    image.thumbnail = obj.filename.split('.')[0] + '_thumb.' + obj.originalname.split('.').pop(); //obj.filename.split('.').pop();
                                    image.thumbnail_square = obj.filename.split('.')[0] + '_thumb_square.' + obj.originalname.split('.').pop();

                                    // image.destination = obj.destination;
                                    // image.file = obj.path;

                                    var thumbnail_setting = [{
                                        width: 800,
                                        heigth: 400,
                                        toFile: image.thumbnail,
                                        thumbnail: "thumbnail_path"
                                    }, {
                                        width: 400,
                                        heigth: 400,
                                        toFile: image.thumbnail_square,
                                        thumbnail: "thumbnail_square"
                                    }];

                                    sharp(obj.path)
                                        .metadata()
                                        .then(function (metadata) {


                                            var settings = []
                                            thumbnail_setting.map(function (setting) {
                                                if (metadata.width < setting.width) setting.width = metadata.width
                                                if (metadata.height < setting.height) setting.height = metadata.height
                                                setting.destination = obj.destination
                                                setting.target = obj.path
                                                settings.push(setting)

                                            })
                                            return settings;
                                        })
                                        .then(function (data) {

                                            Promise
                                                .all(data.map(d => resize(d.width, d.height, d.toFile, d.destination, d.target)))
                                                .then(result => {

                                                    images.push(image);

                                                    resolve();
                                                }).catch(err => {
                                                    console.log(err);
                                                    image.thumbnail_path = null;
                                                    image.thumbnail_square = null;
                                                    images.push(image);
                                                    resolve();
                                                })
                                        });



                                })

                            }

                            promise.push(formImage());
                        }



                        return Promise.all(promise).then(ret => {
                            resolve(images);
                        }).catch(err => {
                            reject(err)
                        })



                    });
                } else
                    return ReE(res, "no uploaded images", 400);




                convertImage.then((images) => {
                   // console.log("get images" + JSON.stringify(images))


                    req.checkBody({
                            'product_name':{
                                notEmpty: true,
                                errorMessage: 'Product name is required',
                                isLength:{
                                    options:{min:1,max:80},
                                    errorMessage:'Maximum 80 characters!'
                                }
                            },
                            'product_desc':{
                                notEmpty: true,
                                errorMessage: 'Product description is required',
                                isLength:{
                                    options:{min:1,max:5000},
                                    errorMessage:'Maximum 5000 characters!'
                                }
                            },
                        'stock':{
                            notEmpty: true,
                            isJSON:{
                                        errorMessage:'Stock require json format'
                                    },
                            errorMessage: 'Stock information is required',
                        },
                        'weight':{
                            notEmpty: true,
                            errorMessage: 'Weight is required',
                            isFloat:{
                                options:{ min: 0.00, max: 100.00 },
                                errorMessage:'Maximum 100 kg and not negative kg'
                            }
                        },
                        'category_id':{
                            notEmpty: true,
                            isInt:{
                                errorMessage: 'Invalid category ID'
                            },
                            errorMessage: 'Category ID is required',
                        },
                        'shipping_id':{
                            notEmpty: true,
                            isInt:{
                                errorMessage: 'Invalid Shipping ID'
                            },
                            errorMessage: 'Shipping ID is required',
                        },
                        'condition':{
                            notEmpty: true,
                            isIn:{
                                options: [['new', 'used']],
                                errorMessage: 'Only new or used',

                            },
                            errorMessage: 'Condition is required',
                        },
                        'self_shipping': {
                            notEmpty: true,
                            isIn: {
                                options: [
                                    ['0', '1']
                                ],
                                errorMessage: 'Only 0 or 1',

                            }
                        },
                        'publish': {
                            notEmpty: true,
                            isIn: {
                                options: [
                                    ['0', '1']
                                ],
                                errorMessage: 'Only 0 or 1',

                            },
                            errorMessage: 'Publish states is required',
                        }

                    });

                    var errors = req.validationErrors(); // get validation error 

                    if (errors) {
                        return ReE(res, errors, 400); // return error
                    }

                    const body = Keyfilter(req.body, ['product_name', 'product_desc', 'stock', 'weight', 'category_id', 'shipping_id', 'condition', 'self_shipping', 'publish']);

                    console.log("key filer"+req.params.shop_id)


                    MarketShop.findOne({ attributes: ['shop_id'],where:{shop_id:req.params.shop_id}}).then(shop =>{ 

                        console.log(" shop"+JSON.stringify(shop))
                        body.product_pic =JSON.stringify(images);
                        body.shop_id =req.params.shop_id;

                        if(shop)
                        return MarketProduct.create(body).then(product => {
                            return ReS(res, 'Successfully created new product',{data:product}, 200);
                        }), function(err) {
                            return ReE(res, err, 400);
                        }

                        else
                            return ReE(res, "no shop found", 400);
                       
                    });



                });

            }


        })
    })



}
module.exports.addProduct = addProduct;



const getProductList = async function (req, res) {
    const seller = req.user;

    let offset = 0;
    let limit = 10;

    if(req.body.limit && parseInt(req.body.limit) > 0)
        limit = parseInt(req.body.limit);
        
    if(req.body.offset && parseInt(req.body.offset) > 0)
        offset = parseInt(req.body.offset);

    var checkShopID = Number.isInteger(parseInt(req.params.shop_id))

    if(!checkShopID)
        return ReE(res,'Invalid Shop ID', 400);

        MarketProduct.findAll({where:{shop_id:parseInt(req.params.shop_id)},attributes: ['product_id','product_pic','product_name','product_desc','sold'],limit:limit,offset:offset}).then(productlist => {

            return ReS(res, '',{data:productlist}, 200);
    
        }).catch((err) => {
    
            return ReE(res,err,400);
        });


}
module.exports.getProductList = getProductList;


const generalProductList = async function (req, res) {
    const seller = req.user;

    let offset = 0;
    let limit = 10;

    if(req.body.limit && parseInt(req.body.limit) > 0)
        limit = parseInt(req.body.limit);
        
    if(req.body.offset && parseInt(req.body.offset) > 0)
        offset = parseInt(req.body.offset);



    req.checkBody({
        'category': {
            notEmpty: true,
            errorMessage: 'category is required',

        }
    })

    var errors = req.validationErrors(); // get validation error 

    if (errors) {
        return ReE(res, errors, 400); // return first error to merchant
    }



        MarketProduct.findAll({where:{publish:1,verify:1,category:req.body.category},attributes: ['product_id','product_pic','product_name'],limit:limit,offset:offset}).then(productlist => {

            return ReS(res, '',{data:productlist}, 200);
    
        }).catch((err) => {
    
            return ReE(res,err,400);
        });


}
module.exports.generalProductList = generalProductList;



const getProductDetail = async function (req, res) {
    let offset = 0;
    let limit = 10;

    if(req.body.limit && parseInt(req.body.limit) > 0)
        limit = parseInt(req.body.limit);
        
    if(req.body.offset && parseInt(req.body.offset) > 0)
        offset = parseInt(req.body.offset);


    var checkShopID = Number.isInteger(parseInt(req.params.shop_id))

    if(!checkShopID)
        return ReE(res,'Invalid Shop ID', 400);

    req.checkBody({
        'product_id': {
            notEmpty: true,
            isInt: {
                errorMessage: 'Invalid product id',
            },
            errorMessage: 'Category ID is required',

        }
    })

    var errors = req.validationErrors(); // get validation error 

    if (errors) {
        return ReE(res, errors, 400); // return first error to merchant
    }


    MarketProduct.findAll({where:{product_id:req.body.product_id},attributes: ['product_pic','product_name','product_desc','stock','category_id','weight','shipping_id','condition','self_shipping','publish','sold'],limit:limit,offset:offset}).then(detail => {

        return ReS(res, '',{data:detail}, 200);

    }).catch((err) => {

        return ReE(res,err,400);
    });



}
module.exports.getProductDetail = getProductDetail;
