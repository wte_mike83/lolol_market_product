const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const Consumer = require('../models').Consumer;


const Device = require('../models').Device;


const consumerAuth = function(passport){
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption_consumer;
    
    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
       
        let err, consumer;

        if(jwt_payload.consumer_id!=null){

            [err, consumer] = await to(Consumer.findById(jwt_payload.consumer_id));

        }else{

            [err, consumer] = await to(Device.findById(jwt_payload.device_id));
            consumer.acc_active = 1;
        }


        if(err) return done(err, false);

        if(consumer) 
        {
            if(consumer.acc_active)
            {
                /*[err, admin_setting] = await to(AdminControl.findOne({where:{operation: 'under_maintenance'} }));
                //console.log(admin_setting)
                if(parseInt(admin_setting.value) == 1)
                    return done('Under Maintenance.', false);*/

                return done(null, consumer);
            }else{
                return done('This account has been deactivated.', false );
            }
        }else{
            return done('Counsumer Not Found!', false);
        }   

        
    }));
}
module.exports.consumerAuth = consumerAuth;




const merchantAuth = function(passport){
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption_merchant;
    
    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
        
        let err, merchant;

        [err, merchant] = await to(Merchant.findById(jwt_payload.merchant_id));


        if(err) return done(err, false);

        if(merchant) 
        {
            if(merchant.acc_active)
            {
                return done(null, merchant);
            }else{
                return done('This account has been deactivated.', false);
            }
        }

        return done('Merchant Not Found!', false);
        
    }));
}
module.exports.merchantAuth = merchantAuth;


const adminAuth = function(passport){
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = CONFIG.jwt_encryption_admin;
    
    passport.use(new JwtStrategy(opts, async function(jwt_payload, done){
        
        let err, admin;

        [err, admin] = await to(Admin.findById(jwt_payload.admin_id));


        if(err) return done(err, false);

        if(admin) 
        {
            if(admin.acc_active)
            {
                /*[err, admin_setting] = await to(AdminControl.findOne({where:{operation: 'under_maintenance'} }));
                //console.log(admin_setting)
                if(parseInt(admin_setting.value) == 1)
                    return done('Under Maintenance.', false);*/

                return done(null, admin);
            }else{
                return done('This account has been deactivated.', false);
            }
        }

        return done('Admin Not Found!', false);
        
    }));
}
module.exports.adminAuth = adminAuth;