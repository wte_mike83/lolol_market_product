var express = require('express');
var router = express.Router();
const passport = require('passport');
const ProductController = require('./../controller/v1/ProductController');
const MarketShop   = require('./../models').MarketShop;


AuthMiddleware = function(req, res, next) {
    require('./../middleware/passport').consumerAuth(passport);
    passport.authenticate('jwt', function(err, user, info) {
        
        if(!user)
        {   
            console.log(req.body);
            console.log(`Authorization::${req.headers.authorization}`);
            if(info)
            {
                console.log(`error_message::${info.message}`);
                return ReE(res,info.message,401);
            }  
            console.log(`error_message::${err}`);  
            return ReE(res,err,401);
                
        }
        req.user = user;
        return next();
  
    })(req, res, next);
  
};

shopVerify = function(req, res, next) {

    MarketShop.findOne({attributes: ['verify'],where:{consumer_id:req.user.consumer_id}}).then(v => {

        if(v)
        {
        if(v.verify == 1)
            return next();
        else
            return ReS(res, 'account still verifying', 200);
        }

            return ReS(res, 'no shop found', 200);

    }).catch((err) => {

        return ReE(res,err,400);
    });



  
  
};



/**
* @api {post} /product/:shop_id/addProduct Add new product
* @apiName Seller add new product

* @apiGroup Seller
*
* @apiHeader {String} Authorization Bearer {Consumer Token}
* @apiHeader {String} Content-Type multipart/form-data
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*
* @apiParam {File} image min 1 and max up to 9
* @apiParam {String} product_name product name
* @apiParam {String} product_desc product description
* @apiParam {Json} stock stock with detail ex:[{"color":"red","limit":"1"},{"color":"blue","limit":"1"}]
* @apiParam {Float} price price (RM)
* @apiParam {Float} weight weight(kg) 
* @apiParam {Int} category_id category id (refer category list)
* @apiParam {Int} shipping_id shipping id (refer shipping company list)
* @apiParam {String} condition new or used
* @apiParam {Int} self_shipping 0 or 1
* @apiParam {Int} publish 0 or 1


* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
* {
        "data": {
            "createdAt": "2019-01-23 18:41:16",
            "product_id": 13,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "stock": "{\"as\":\"asd\"}",
            "weight": "12",
            "category_id": "1",
            "shipping_id": "1",
            "condition": "used",
            "publish": "1",
            "self_shipping": "1",
            "product_pic": "[{\"review_img\":\"zn8bzwoms892tbo.JPG\",\"checkSum\":\"9e89818a06727ad8c5694a72e39f118a\",\"thumbnail\":\"zn8bzwoms892tbo_thumb.JPG\",\"thumbnail_square\":\"zn8bzwoms892tbo_thumb_square.JPG\",\"destination\":\"../../../opt/ieat-static/images/product/1\",\"file\":\"../../../opt/ieat-static/images/product/1/zn8bzwoms892tbo.JPG\"},{\"review_img\":\"wv6hgpy10c0l25n.jpg\",\"checkSum\":\"998f50c21256f908e8c378601963e238\",\"thumbnail\":\"wv6hgpy10c0l25n_thumb.jpg\",\"thumbnail_square\":\"wv6hgpy10c0l25n_thumb_square.jpg\",\"destination\":\"../../../opt/ieat-static/images/product/1\",\"file\":\"../../../opt/ieat-static/images/product/1/wv6hgpy10c0l25n.jpg\"},{\"review_img\":\"8k4il5np2chx5zq.JPG\",\"checkSum\":\"e9c015e87d5e8a2dd584517977db5aae\",\"thumbnail\":\"8k4il5np2chx5zq_thumb.JPG\",\"thumbnail_square\":\"8k4il5np2chx5zq_thumb_square.JPG\",\"destination\":\"../../../opt/ieat-static/images/product/1\",\"file\":\"../../../opt/ieat-static/images/product/1/8k4il5np2chx5zq.JPG\"}]",
            "shop_id": "1",
            "updatedAt": "2019-01-23T10:41:16.802Z"
        },
        "success": true,
        "message": "Successfully created new product"
    }
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not consumer)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

* @apiErrorExample No image upload
*     HTTP/1.1 400 Bad Request
    *     {
        "success": false,
        "message": "no uploaded images"
    }
*

* @apiErrorExample Validation Error
*     HTTP/1.1 400 Bad Request
{
        "success": false,
        "message": [
            {
                "location": "body",
                "param": "product_name",
                "msg": "Product name is required"
            },
            {
                "location": "body",
                "param": "product_name",
                "msg": "Maximum 80 characters!"
            },
            {
                "location": "body",
                "param": "product_desc",
                "msg": "Product description is required"
            },
            {
                "location": "body",
                "param": "product_desc",
                "msg": "Maximum 5000 characters!"
            },
            {
                "location": "body",
                "param": "stock",
                "msg": "Stock information is required"
            },
            {
                "location": "body",
                "param": "stock",
                "msg": "Stock require json format"
            },
            {
                "location": "body",
                "param": "weight",
                "msg": "Weight is required"
            },
            {
                "location": "body",
                "param": "weight",
                "msg": "Maximum 100 kg and not negative kg"
            },
            {
                "location": "body",
                "param": "category_id",
                "msg": "Category ID is required"
            },
            {
                "location": "body",
                "param": "category_id",
                "msg": "Invalid category ID"
            },
            {
                "location": "body",
                "param": "shipping_id",
                "msg": "Shipping ID is required"
            },
            {
                "location": "body",
                "param": "shipping_id",
                "msg": "Invalid Shipping ID"
            },
            {
                "location": "body",
                "param": "condition",
                "msg": "Condition is required"
            },
            {
                "location": "body",
                "param": "condition",
                "msg": "Only new or used"
            },
            {
                "location": "body",
                "param": "self_shipping",
                "msg": "Invalid value"
            },
            {
                "location": "body",
                "param": "self_shipping",
                "msg": "Only 0 or 1"
            },
            {
                "location": "body",
                "param": "publish",
                "msg": "Publish states is required"
            },
            {
                "location": "body",
                "param": "publish",
                "msg": "Only 0 or 1"
            }
        ]
    }


*/
router.post('/:shop_id/addProduct',AuthMiddleware,shopVerify,ProductController.addProduct); 



/**
* @api {post} /product/:shop_id/getProductList  Product list
* @apiName Product list

* @apiGroup Seller
*
* @apiHeader {String} Authorization Bearer {Consumer Token}
* @apiHeader {String} Content-Type application/x-www-form-urlencoded
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*


* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
{
    "data": [
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 43,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "sold": 0
        },
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/cz4u9s1p3sdrra1.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/cz4u9s1p3sdrra1_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/cz4u9s1p3sdrra1_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/qie3x7f7tnxh560.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/qie3x7f7tnxh560_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/qie3x7f7tnxh560_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/1a1f9kv7mn5h1mp.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/1a1f9kv7mn5h1mp_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/1a1f9kv7mn5h1mp_thumb_square.JPG"
                }
            ],
            "product_id": 48,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "sold": 0
        }
    ],
    "success": true,
    "message": ""
}
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not consumer)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

*/

router.post('/:shop_id/getProductList',AuthMiddleware,ProductController.getProductList); 

/**
* @api {post} /product/generalProductList  Product list
* @apiName Product list

* @apiGroup General
*
* @apiHeader {String} Authorization Bearer {Consumer Token}
* @apiHeader {String} Content-Type application/x-www-form-urlencoded
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*
* @apiParam {String} category product category
* @apiParam {String} sort sort with name
* @apiParam {String} sortby sort by asc or desc

* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
* HTTP/1.1 200 OK
{
    "data": [
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 47,
            "product_name": "asdasasd"
        },
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 49,
            "product_name": "asdasasd"
        },
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 43,
            "product_name": "asdasasd"
        }
    ],
    "success": true,
    "message": ""
}
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not consumer)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

*/
router.post('/generalProductList',AuthMiddleware,ProductController.generalProductList); 



/**
* @api {post} /product/:shop_id/getProductDetail Product detail
* @apiName Product detail

* @apiGroup Seller
*
* @apiHeader {String} Authorization Bearer {Consumer Token}
* @apiHeader {String} Content-Type application/x-www-form-urlencoded
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*
* @apiParam {Int} product_id product id


* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
{
    "data": [
        {
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/undefined/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "stock": "{\"as\":\"asd\"}",
            "category_id": 1,
            "weight": "12.00",
            "shipping_id": 1,
            "condition": "used",
            "self_shipping": 1,
            "publish": 1,
            "sold": 0
        }
    ],
    "success": true,
    "message": ""
}
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not consumer)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample Invalid product_id
*     HTTP/1.1 400 Bad Request
{
    "success": false,
    "message": [
        {
            "location": "body",
            "param": "product_id",
            "msg": "Category ID is required"
        },
        {
            "location": "body",
            "param": "product_id",
            "msg": "Invalid product id"
        }
    ]
}


*/
router.post('/:shop_id/getProductDetail',AuthMiddleware,ProductController.getProductDetail); 




module.exports = router;