var express = require('express');
var router = express.Router();
const passport = require('passport');
const AdminController = require('./../controller/v1/AdminController');
const MarketShop   = require('./../models').MarketShop;


AuthMiddleware = function(req, res, next) {
    require('./../middleware/passport').adminAuth(passport);
    passport.authenticate('jwt', function(err, user, info) {
        
        if(!user)
        {   
            console.log(req.body);
            console.log(`Authorization::${req.headers.authorization}`);
            if(info)
            {
                console.log(`error_message::${info.message}`);
                return ReE(res,info.message,401);
            }  
            console.log(`error_message::${err}`);  
            return ReE(res,err,401);
                
        }
        req.user = user;
        return next();
  
    })(req, res, next);
  
};

shopVerify = function(req, res, next) {

    MarketShop.findOne({attributes: ['verify'],where:{consumer_id:req.user.consumer_id}}).then(v => {

        if(v)
        {
        if(v.verify == 1)
            return next();
        else
            return ReS(res, 'account still verifying', 200);
        }

            return ReS(res, 'no shop found', 200);

    }).catch((err) => {

        return ReE(res,err,400);
    });

  
};

router.post('/test',AuthMiddleware); 



/**
* @api {get} /admin/pendingProduct  Admin get pending product list
* @apiName  pending product list

* @apiGroup Admin
*
* @apiHeader {String} Authorization Bearer {Admin Token}
* @apiHeader {String} Content-Type application/x-www-form-urlencoded
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*

* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
{
    "data": [
        {
            "createdAt": "2019-01-24 12:21:28",
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 43,
            "shop_id": 1,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "price": "10.00",
            "stock": "{\"as\":\"asd\"}",
            "category_id": 1,
            "category": "electronic",
            "weight": "12.00",
            "shipping_id": 1,
            "condition": "used",
            "self_shipping": 1,
            "publish": 1,
            "sold": 1,
            "verify": 0,
            "updatedAt": "2019-01-24T04:21:28.000Z"
        },
        {
            "createdAt": "2019-01-24 12:21:28",
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/2swhh8bmc5s6873_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/y37f0c8jr7ca0xr_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/u59hyks2vno5ff9_thumb_square.JPG"
                }
            ],
            "product_id": 47,
            "shop_id": 1,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "price": "11.00",
            "stock": "{\"as\":\"asd\"}",
            "category_id": 1,
            "category": "electronic",
            "weight": "12.00",
            "shipping_id": 1,
            "condition": "used",
            "self_shipping": 1,
            "publish": 1,
            "sold": 12,
            "verify": 0,
            "updatedAt": "2019-01-24T04:21:28.000Z"
        },
        {
            "createdAt": "2019-01-24 12:21:33",
            "product_pic": [
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/cz4u9s1p3sdrra1.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/cz4u9s1p3sdrra1_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/cz4u9s1p3sdrra1_thumb_square.JPG"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/qie3x7f7tnxh560.jpg",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/qie3x7f7tnxh560_thumb.jpg",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/qie3x7f7tnxh560_thumb_square.jpg"
                },
                {
                    "product_img": "http://129.126.133.187:4001/images/product/1/1a1f9kv7mn5h1mp.JPG",
                    "thumbnail": "http://129.126.133.187:4001/images/product/1/1a1f9kv7mn5h1mp_thumb.JPG",
                    "thumbnail_square": "http://129.126.133.187:4001/images/product/1/1a1f9kv7mn5h1mp_thumb_square.JPG"
                }
            ],
            "product_id": 48,
            "shop_id": 1,
            "product_name": "asdasasd",
            "product_desc": "sdasdasdasdasda",
            "price": "12.00",
            "stock": "{\"as\":\"asd\"}",
            "category_id": 1,
            "category": null,
            "weight": "12.00",
            "shipping_id": 1,
            "condition": "used",
            "self_shipping": 1,
            "publish": 1,
            "sold": 5,
            "verify": 0,
            "updatedAt": "2019-01-24T04:21:33.000Z"
        }
    ],
    "success": true,
    "message": ""
}
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not admin)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample no product found
*     HTTP/1.1 400 Bad Request
{
    "success": true,
    "message": "no product found"
}


*/

router.get('/pendingProduct',AuthMiddleware,AdminController.pendingProduct); 




/**
* @api {get} /admin/approveProduct  Admin approce product
* @apiName  Admin approce product

* @apiGroup Admin
*
* @apiHeader {String} Authorization Bearer {Admin Token}
* @apiHeader {String} Content-Type application/x-www-form-urlencoded
*
* @apiHeaderExample {json} Request-Example:
* Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImp0aSI6IjRmMWcyM2ExMmFhIn0.eyJpc3MiOiJodHRwczpcL1wvYXBpLmlrb2Z4LmNvbSIsImF1ZCI6Imh0dHBzOlwvXC9hcGkuaWtvZnguY29tIiwianRpIjoiNGYxZzIzYTEyYWEiLCJpYXQiOjE0NjE3ODA3MDMsIm5iZiI6MTQ2MTc4MDcwNCwiZXhwIjoxNDYxNzg0MzAzLCJ1aWQiOjgsImVtYWlsIjoibWlrZUBpa29meC5jb20iLCJuYW1lIjoiXHU3YzczXHU1MTRiXHU2NzRlIn0.5kRcloP0E4Zg6gZivzOebZ1n-gNY7BVGzkAh8iteyK-m8TQ1THfRtHxgqOUG0SsJK18ckdi_9emkrhfaqR36yQ
* Content-Type: application/x-www-form-urlencoded
*
* @apiParam {String} product_id product id 


* @apiSuccess {Boolean} success Success Status
* @apiSuccess {string} message Message


* @apiSuccessExample Success-Response:
{
    "success": true,
    "message": "Update Sucessful"
}
*
*
* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample User Type Invalid (not admin)
*     HTTP/1.1 400 Bad Request
*     {
*       "success": false,
*       "message": "Invalid User"
*     }

* @apiError {string} message Error Message
* @apiError {int} status_code Error Status Code

* @apiErrorExample validation error
*     HTTP/1.1 400 Bad Request
{
    "success": false,
    "message": [
        {
            "location": "body",
            "param": "product_id",
            "msg": "Invalid value"
        },
        {
            "location": "body",
            "param": "product_id",
            "msg": "Invalid product_id"
        }
    ]
}


*/



router.post('/approveProduct',AuthMiddleware,AdminController.approveProduct); 

module.exports = router;